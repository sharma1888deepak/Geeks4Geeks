package com.hello;

import java.util.Scanner;

class ConcateString {
	public static void main (String[] args) {
		Scanner sc = new Scanner(System.in);
		int i =sc.nextInt();
		String[] sa=new String[i*2];
		for(int j=0;j<i*2;j++){
			Scanner sc1 = new Scanner(System.in);
			sa[j]= sc1.nextLine();
		}
		concatAndReverseStrings(sa);		
	}
	
	public static void concatAndReverseStrings(String[] sa){
			
		String[] concatArray=new String[sa.length/2];
		for(int i=0,j=0;i<=sa.length/2;i=i+2,j++){
			concatArray[j]=sa[i]+sa[i+1];
		}
		
		for(int i=0;i<concatArray.length;i++){
			StringBuilder input1 = new StringBuilder();
			input1.append(concatArray[i]);
		     System.out.println(input1.reverse());
		}
		
	}
}