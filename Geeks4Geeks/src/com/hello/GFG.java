package com.hello;

import java.util.*;
import java.lang.*;
import java.io.*;


//Reverse a String
class GFG {
	public static void main (String[] args) {
		//code
		Scanner scan = new Scanner(System.in);
		int t = scan.nextInt();
		for(int i = 0; i < t; i++){
		    String a = scan.next();
		    String b = scan.next();
		    String c = a.concat(b);
		    StringBuilder sc = new StringBuilder(c);
		    sc.reverse();
		    c = sc.toString();
		    System.out.println(c);
		}
	}
}