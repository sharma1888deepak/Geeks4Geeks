package com.output;

public class TestFinalize2
{
    public static void main(String[] args) throws InterruptedException
    {
    	TestFinalize2 t = new TestFinalize2();
             
        // making t eligible for garbage collection
        t = null; 
             
        // calling garbage collector
        System.gc(); 
             
        // waiting for gc to complete
        Thread.sleep(1000); 
     
        System.out.println("end main");
    }
 
    @Override
    protected void finalize() 
    {
        System.out.println("finalize method called");
        System.out.println(10/0);
    }
     
}

/*Output:

finalize method called
end main*/
